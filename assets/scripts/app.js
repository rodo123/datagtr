//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VARIABLES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

var features_global;
var nodos_global;
var devices_global;
var data_total;

var labels_day = [];
var datasets_data = [];
var myChart;
var ctx;

var country;

var feature_id;
var device_id;

var date_start;
var dates = [];
var date;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DATA PRUEBA
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

var months = ["jan","feb","mar","apr","may","jun","jul","aug","sep","nov","dec"]
//features_global = {"features": [{"feature_name":"voltage", "feature_id":"1"},{"feature_name":"solar", "feature_id":"2"}]}
//nodos_global = {"nodos": [{"nodo_name":"Iquitos", "nodo_id":"1", "nodo_lat":"-3.6130167", "nodo_lon":"-73.17223611111112"},{"nodo_name":"Mazán", "nodo_id":"2", "nodo_lat":"-3.4999722", "nodo_lon":"-73.0911111111111"}]}
//devices_global = {"devices": [{"device_name":"bm1", "device_id":"1"},{"device_name":"bm2", "device_id":"2"}]}
//country = {"countries": [{"country_name":"Perú", "country_id":"1"},{"country_name":"Francia", "country_id":"2"}]}


/*data_total = {
  "features": [
    { "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"01:00", "feature_id":"1", "feature_value":"220", "device_id":"1"},
	{ "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"01:00", "feature_id":"2", "feature_value":"320", "device_id":"1"},
    { "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"02:00", "feature_id":"1", "feature_value":"210", "device_id":"1"},
    { "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"02:00", "feature_id":"2", "feature_value":"220", "device_id":"1"},
	{ "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"03:00", "feature_id":"1", "feature_value":"270", "device_id":"1"},
    { "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"03:00", "feature_id":"2", "feature_value":"400", "device_id":"1"},
	{ "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"04:00", "feature_id":"1", "feature_value":"200", "device_id":"1"},
	{ "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"04:00", "feature_id":"2", "feature_value":"210", "device_id":"1"},
	{ "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"01:00", "feature_id":"1", "feature_value":"220", "device_id":"1"},
	{ "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"01:00", "feature_id":"2", "feature_value":"140", "device_id":"1"},
    { "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"02:00", "feature_id":"1", "feature_value":"230", "device_id":"1"},
    { "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"02:00", "feature_id":"2", "feature_value":"130", "device_id":"1"},
	{ "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"03:00", "feature_id":"1", "feature_value":"240", "device_id":"1"},
    { "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"03:00", "feature_id":"2", "feature_value":"120", "device_id":"1"},
	{ "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"04:00", "feature_id":"1", "feature_value":"250", "device_id":"1"},
	{ "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"04:00", "feature_id":"2", "feature_value":"110", "device_id":"1"},
	{ "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"05:00", "feature_id":"1", "feature_value":"22", "device_id":"2"},
	{ "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"05:00", "feature_id":"2", "feature_value":"32", "device_id":"2"},
    { "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"06:00", "feature_id":"1", "feature_value":"21", "device_id":"2"},
    { "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"06:00", "feature_id":"2", "feature_value":"22", "device_id":"2"},
	{ "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"07:00", "feature_id":"1", "feature_value":"27", "device_id":"2"},
    { "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"07:00", "feature_id":"2", "feature_value":"40", "device_id":"2"},
	{ "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"08:00", "feature_id":"1", "feature_value":"20", "device_id":"2"},
	{ "nodo_id":"1", "feature_date":"2019-06-01", "feature_time":"08:00", "feature_id":"2", "feature_value":"21", "device_id":"2"},
	{ "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"05:00", "feature_id":"1", "feature_value":"22", "device_id":"2"},
	{ "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"05:00", "feature_id":"2", "feature_value":"14", "device_id":"2"},
    { "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"06:00", "feature_id":"1", "feature_value":"23", "device_id":"2"},
    { "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"06:00", "feature_id":"2", "feature_value":"13", "device_id":"2"},
	{ "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"07:00", "feature_id":"1", "feature_value":"24", "device_id":"2"},
    { "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"07:00", "feature_id":"2", "feature_value":"12", "device_id":"2"},
	{ "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"08:00", "feature_id":"1", "feature_value":"25", "device_id":"2"},
	{ "nodo_id":"2", "feature_date":"2019-06-01", "feature_time":"08:00", "feature_id":"2", "feature_value":"11", "device_id":"2"},
	{ "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"01:00", "feature_id":"1", "feature_value":"220", "device_id":"1"},
	{ "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"01:00", "feature_id":"2", "feature_value":"320", "device_id":"1"},
    { "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"02:00", "feature_id":"1", "feature_value":"210", "device_id":"1"},
    { "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"02:00", "feature_id":"2", "feature_value":"220", "device_id":"1"},
	{ "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"03:00", "feature_id":"1", "feature_value":"270", "device_id":"1"},
    { "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"03:00", "feature_id":"2", "feature_value":"400", "device_id":"1"},
	{ "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"04:00", "feature_id":"1", "feature_value":"200", "device_id":"1"},
	{ "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"04:00", "feature_id":"2", "feature_value":"210", "device_id":"1"},
	{ "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"01:00", "feature_id":"1", "feature_value":"220", "device_id":"1"},
	{ "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"01:00", "feature_id":"2", "feature_value":"140", "device_id":"1"},
    { "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"02:00", "feature_id":"1", "feature_value":"230", "device_id":"1"},
    { "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"02:00", "feature_id":"2", "feature_value":"130", "device_id":"1"},
	{ "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"03:00", "feature_id":"1", "feature_value":"240", "device_id":"1"},
    { "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"03:00", "feature_id":"2", "feature_value":"120", "device_id":"1"},
	{ "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"04:00", "feature_id":"1", "feature_value":"250", "device_id":"1"},
	{ "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"04:00", "feature_id":"2", "feature_value":"110", "device_id":"1"},
	{ "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"05:00", "feature_id":"1", "feature_value":"22", "device_id":"2"},
	{ "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"05:00", "feature_id":"2", "feature_value":"32", "device_id":"2"},
    { "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"06:00", "feature_id":"1", "feature_value":"21", "device_id":"2"},
    { "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"06:00", "feature_id":"2", "feature_value":"22", "device_id":"2"},
	{ "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"07:00", "feature_id":"1", "feature_value":"27", "device_id":"2"},
    { "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"07:00", "feature_id":"2", "feature_value":"40", "device_id":"2"},
	{ "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"08:00", "feature_id":"1", "feature_value":"20", "device_id":"2"},
	{ "nodo_id":"1", "feature_date":"2019-06-02", "feature_time":"08:00", "feature_id":"2", "feature_value":"21", "device_id":"2"},
	{ "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"05:00", "feature_id":"1", "feature_value":"22", "device_id":"2"},
	{ "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"05:00", "feature_id":"2", "feature_value":"14", "device_id":"2"},
    { "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"06:00", "feature_id":"1", "feature_value":"23", "device_id":"2"},
    { "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"06:00", "feature_id":"2", "feature_value":"13", "device_id":"2"},
	{ "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"07:00", "feature_id":"1", "feature_value":"24", "device_id":"2"},
    { "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"07:00", "feature_id":"2", "feature_value":"12", "device_id":"2"},
	{ "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"08:00", "feature_id":"1", "feature_value":"25", "device_id":"2"},
	{ "nodo_id":"2", "feature_date":"2019-06-02", "feature_time":"08:00", "feature_id":"2", "feature_value":"11", "device_id":"2"}
	]
 }
*/
 
nodos_global = getNodos();
devices_global = getDevices();
features_global = getFeatures();
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// UTILS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Función fechas y horas
function getTimes() {
  var d = new Date();
  var month = d.getMonth();
  var day = d.getUTCDate();
  var year = d.getFullYear();
  var month_real = months[month];
  var date = month_real+"/"+day+"/"+year;
  return date;
}


// Función para borrar valores repetidos en un array
Array.prototype.unique=function(a){
  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
});


// Función para generar colores random para el gráfico 
function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}


// Agrega todos los nodos a una lista
$(function() {
    $.each(nodos_global.nodos, function(i, option) {
		console.log();
        $('#sel').append($('<option/>').attr("value", option.nodo_id).text(option.nodo_name));
    });
})


// Agrega todos los equipos a una lista
$(function() {
    $.each(devices_global.devices, function(i, option) {
		console.log();
        $('#sel2').append($('<option/>').attr("value", option.device_id).text(option.device_name));
    });
})


// Agrega todos los paises a una lista
$(function() {
    $.each(country.countries, function(i, option) {
		console.log();
        $('#ddlCountry').append($('<option/>').attr("value", option.country_id).text(option.name));
    });
})


// Agrega todos los indicadores a una lista
$(function() {
    $.each(features_global.features, function(i, option) {
		console.log();
        $('#indicadores').append($('<option/>').attr("value", option.feature_id).text(option.feature_name));
    });
})


// Agrega todo los equipos a una lista general
$(function() {
    $.each(devices_global.devices, function(i, option) {
		console.log();
        $('#equipos_lista').append($('<option/>').attr("value", option.device_id).text(option.device_name));
    });
})

// Crea el gráfico General por Rango de Fechas
$(function() {
  $('input[name="daterange"]').daterangepicker({
    opens: 'left'
  }, function(start, end, label) {
	    var date_1 = end.format('MM-DD-YY');
		var date_2 = start.format('MM-DD-YY');
		
		var dates_1 = date_1.split("-");
		var dates_2 = date_2.split("-");
		
		var datas_real_1 = months[parseInt(dates_1[0])-1];
		date = datas_real_1+"/"+dates_1[1]+"/"+dates_1[2];
		
		var datas_real_2 = months[parseInt(dates_2[0])-1];
		date_start = datas_real_2+"/"+dates_2[1]+"/"+dates_2[2]; 
		console.log(date_start);
		console.log(date);
		createGra();
		showDataGeneral(1,3);
    //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });
});


function eliminarPorName(name){
	//console.log("Eliminar");
	//console.log(features_global);
	/*for (i in features_global.features){
		console.log(name);
		console.log(features_global.features[i].feature_name);
		if (features_global.features[i].feature_name == name){
			console.log("Entre");
			delete features_global.features[i];
		}
	}*/
	console.log(features_global);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// LOGIN
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

$('.toggle').click(function(){
  $('.formulario').animate({
      height: "toggle",
      'padding-top': 'toggle',
      'padding-bottom': 'toggle',
      opacity: 'toggle'
  }, "slow");
});


country = getCountries(); 


var views = function(){
  
  var $navItem = $('.nav-item');
  var $a = $(".inf-dashboard-js");
  $navItem.on("click",function() {
    $navItem.removeClass("active");
    $(this).addClass('active');
  })

  $a.on("click",function() {
	console.log("CORRECTO");
    var atributo = $(this).attr('href');
    if(atributo=="#report-general-js"){
      $("#report-especifico-js").hide();
      $("#report-general-js").show();
      $("#dashboard-js").hide();
    }
    if(atributo=="#report-especifico-js"){
		
      $("#dashboard-js").hide();
      $("#report-general-js").hide();
      $("#report-especifico-js").show();
    }
    if(atributo=="#dashboard-js"){
      $("#dashboard-js").show();
      $("#report-general-js").hide();
      $("#report-especifico-js").hide();
    }
  })
}


function check(){
	var user = document.forms["login"]["usuario"].value;
	var contraseña = document.forms["login"]["contraseña"].value;
	var flag = 0;
	var result = getLogin(user,contraseña);
	for (i in result){
		if (i == "user_id"){
			flag = result[i];
		}
	}
	if (flag == 0){
		alert("Usuario o Contraseña incorrecto");
		return false;
	}
}


function cuenta(){

	var nombres = document.forms["registro"]["nombres"].value;
	var apellidos = document.forms["registro"]["apellidos"].value;
	var correo = document.forms["registro"]["correo"].value;
	var contraseña = document.forms["registro"]["contraseña_cuenta"].value;
	var celular = document.forms["registro"]["celular"].value;
	var pais = document.forms["registro"]["country"].value;
	var flag = 0;
	var result = account(nombres,apellidos,correo,contraseña,celular,pais);
	
	for (i in result){
		if (i == "user_id"){
			flag = result[i];
		}
	}
	
	if (flag == 0){
		alert("Usuario ya existente");
		return false;
	}
}


function getContraseña(){
	alert("Favor de contactarse con River Quispe al correo: riverqt@gmail.com");
}


views();
$("#report-general-js").hide();
$("#report-especifico-js").hide();


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MAPA
//////////////////////////////////////////////////////////////////////////////////////////////////////////////



function mapa_geo(){
	
	var cities = L.layerGroup();

	for (var i = 0 ; i < 16 ; i++){
		L.marker([parseFloat(nodos_global.nodos[i].nodo_lat), parseFloat(nodos_global.nodos[i].nodo_lon)]).bindPopup("<b>"+nodos_global.nodos[i].nodo_name).addTo(cities);
	}
	
	var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

	var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
		streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr});
		
	var map = L.map('mapid', {
		center: [-1.547552, -74.539380],
		zoom: 7,
		layers: [grayscale, cities]
	});

	var baseLayers = {
		"Grayscale": grayscale,
		"Streets": streets
	};

	var overlays = {
		"Nodos": cities
	};

	L.control.layers(baseLayers, overlays).addTo(map);
	
}

mapa_geo();


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MÉTODOS GENERADORES
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

function getData_dailyEspe(){
	createGra();
	showData(1,1);
}


function getData_dailyGeneral(){
	myChart.destroy();
	showDataGeneral(1,1);
}


function getData_monthEspe(){
	createGra();
	showData(1,2);
}


function getData_monthGeneral(){
	myChart.destroy();
	showDataGeneral(1,2);
}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MÉTODOS POST
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Método POST para login
function getLogin(user,contraseña){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/login",
	data: JSON.stringify({"email":user, "password":contraseña}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data;
	console.log(result);
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


// Método POST para registrar a un usuario
function account(nombres,apellidos,correo,contraseña,celular,pais){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/account",
	data: JSON.stringify({"first_name":nombres, "last_name":apellidos, "email":correo, "password":contraseña, "phone":celular, "country_id":pais}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data;
	console.log(result);
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


// Método POST para conseguir los paises
function getCountries(){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/country",
	data: JSON.stringify({"status":"ok"}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


// Método POST para conseguir los datos de los nodos
function getNodos(){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/nodo",
	data: JSON.stringify({"status":"ok"}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data;
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


// Método POST para conseguir los datos de los equipos
function getDevices(){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/device",
	data: JSON.stringify({"status":"ok"}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data;
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


// Método POST para conseguir los datos de los parámetros (voltaje, db, bytes)
function getFeatures(){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/feature",
	data: JSON.stringify({"status":"ok"}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data;
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


// Método POST para conseguir los datos del día en especifico
function getDadaDayEsp(date,nodo_id,device_id){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/data_daily_esp",
	data: JSON.stringify({"date":date, "nodo_id":nodo_id, "device_id":device_id}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data;
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


// Método POST para conseguir los datos del día
function getDadaDayGen(date,feature_id,device_id){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/data_daily_general",
	data: JSON.stringify({"date":date, "feature_id":feature_id, "device_id":device_id}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data;
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


// Método POST para conseguir los datos del mes
function getDataMonthGen(date_start,date_today,feature_id,device_id){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/data_month_general",
	data: JSON.stringify({"date_start":date_start,"date_today":date_today,"feature_id":feature_id, "device_id":device_id}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data;
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


// Método POST para conseguir los datos del mes
function getDataMonthEsp(date_start,date_today,nodo_id,device_id){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/data_month_esp",
	data: JSON.stringify({"date_start":date_start,"date_today":date_today,"nodo_id":nodo_id, "device_id":device_id}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data;
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


// Método POST para conseguir los datos en un rango determinado
function getDataRangeGen(date_start,date_today,feature_id,device_id){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/data_range_general",
	data: JSON.stringify({"date_start":date_start,"date_today":date_today,"feature_id":feature_id, "device_id":device_id}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


// Método POST para conseguir los datos en un rango determinado
function getDataRangeEsp(date_start,date_today,nodo_id,device_id){
	var result="";
	$.ajax({
	type: 'POST',
	async: false,
	crossDomain: true,
	url: "http://192.168.0.25:5000/data_range_esp",
	data: JSON.stringify({"date_start":date_start,"date_today":date_today,"nodo_id":nodo_id, "device_id":device_id}),
	error: function(e) {
	console.log(e);
	},
	success: function (data) { 
	result = data
	},
	dataType: "json",
	contentType: "application/json"
	});
	return result;
}


function showData(flag1,flag2){
	createGra();
	myChart.destroy();
	console.log("Destruye el gráfico");
	date = "apr/04/2019";
	var nodo_id = document.getElementById("sel").value;
	var device_id = document.getElementById("sel2").value;
	var data_total_parcial = [];
	

	if (flag2 == 1){
		data_total = getDadaDayEsp(date,nodo_id,device_id);
	}
	
	if (flag2 == 2){
		dates = date.split("/");
		date_start = dates[0]+"/01/"+dates[2];
		data_total = getDataMonthEsp(date_start,date,nodo_id,device_id);
	}
	
	if (flag2 == 3){
		data_total = getDataRangeEsp(date_start,date,nodo_id,device_id);
	}
	
	for (i in data_total.features){
		if (data_total.features[i].nodo_id == nodo_id && data_total.features[i].device_id == device_id){
			data_total_parcial.push(data_total.features[i])
		}
	}
	var data_total_select = {"features": data_total_parcial};
	//console.log("data_total_select");
	//console.log(data_total_select);
	getDataStatistics(data_total_select,flag1);
	createGra();
}


function showDataGeneral(flag1,flag2){
	date = "apr/04/2019";
	feature_id = document.getElementById("indicadores").value;
	device_id = document.getElementById("equipos_lista").value;
	if (flag2 == 1){
		data_total = getDadaDayGen(date,feature_id,device_id);
	}
	
	if (flag2 == 2){
		dates = date.split("/");
		date_start = dates[0]+"/01/"+dates[2];
		data_total = getDataMonthGen(date_start,date,feature_id,device_id);
	}
	
	if (flag2 == 3){
		data_total = getDataRangeGen(date_start,date,feature_id,device_id);
	}
	
	var data_total_parcial = [];
	
	for (i in data_total.features){
		if (data_total.features[i].feature_id == feature_id && data_total.features[i].device_id == device_id){
			data_total_parcial.push(data_total.features[i])
		}
	}
	var data_total_select = {"features": data_total_parcial};
	//console.log("data_total_select");
	//console.log(data_total_select);
	
	getDataStatisticsGeneral(data_total_select,flag1);
	createGraGeneral();
}


function getDataStatisticsGeneral(data_json,flag){
	
	var features_total = [];
	var features_chart = [];
	var data_real = [];
	
	// Conseguimos todos los feature_id y lo almacenamos en un array
	for (i in data_json.features) {
	  features_total.push(data_json.features[i].nodo_id);
	}

	features_total.pop();
	features_total = features_total.unique();
	//console.log("features_total");
	//console.log(features_total);
	
	
	// Conseguimos los nombres de los features a partir del array de nodo_id
	for (i in features_total){
		for (j in nodos_global.nodos){
			if (features_total[i] == nodos_global.nodos[j].nodo_id){
				features_chart.push(nodos_global.nodos[j].nodo_name);
			}
		}
	}
	//console.log("features_chart");
	//console.log(features_chart);

	labels_day = [];
	// Conseguimos todos los labels (fecha y tiempo) del gráfico 
	for (i in data_json.features) {
	  labels_day.push(data_json.features[i].feature_date+" "+data_json.features[i].feature_time);
	}
	
	labels_day.pop();
	labels_day = labels_day.unique();
	labels_day = labels_day.sort();
	//console.log("labels_day");
	//console.log(labels_day);

	// Conseguimos los valores a poner en el cuadro por parámetro (ejem: voltage = 220)
	for (x in features_total){
		var real_value = [];
		for (i in labels_day){
			for (j in data_json.features){
				if (features_total[x] == data_json.features[j].nodo_id){
					if (labels_day[i] == data_json.features[j].feature_date+" "+data_json.features[j].feature_time){
						real_value.push(data_json.features[j].feature_value);
					}
				}
			}
		}
		data_real.push(real_value);
	}
	data_real.pop();
	//console.log("data_real");
	//console.log(data_real);
	
	var p;
	// Conseguimos el array global para llenar el gráfico
	//console.log("datasets_data");
	//console.log(datasets_data);
	datasets_data = [];
	for (i in data_real){
		if (flag == 0){
			p = {data: data_real[i], label: features_chart[i], borderColor: getRandomColor(), fill: false,hidden: true};
		} else {
			p = {data: data_real[i], label: features_chart[i], borderColor: getRandomColor(), fill: false};
		}
		datasets_data.push(p);
	}
	datasets_data.pop();
	//console.log("datasets_data");
	//console.log(datasets_data);
}


function getDataStatistics(data_json,flag){
	
	var features_total = [];
	var features_chart = [];
	var data_real = [];
	
	// Conseguimos todos los feature_id y lo almacenamos en un array
	for (i in data_json.features) {
	  features_total.push(data_json.features[i].feature_id);
	}

	features_total.pop();
	features_total = features_total.unique();
	console.log("features_total");
	console.log(features_total);
	
	
	// Conseguimos los nombres de los features a partir del array de feature_id
	for (i in features_total){
		for (j in features_global.features){
			if (features_total[i] == features_global.features[j].feature_id){
				features_chart.push(features_global.features[j].feature_name);
			}
		}
	}
	console.log("features_chart");
	console.log(features_chart);

	labels_day = [];
	// Conseguimos todos los labels (fecha y tiempo) del gráfico 
	for (i in data_json.features) {
	  labels_day.push(data_json.features[i].feature_date+" "+data_json.features[i].feature_time);
	}
	
	labels_day.pop();
	labels_day = labels_day.unique();
	labels_day = labels_day.sort();
	console.log("labels_day");
	console.log(labels_day);

	// Conseguimos los valores a poner en el cuadro por parámetro (ejem: voltage = 220)
	for (x in features_total){
		var real_value = [];
		for (i in labels_day){
			for (j in data_json.features){
				if (features_total[x] == data_json.features[j].feature_id){
					if (labels_day[i] == data_json.features[j].feature_date+" "+data_json.features[j].feature_time){
						real_value.push(data_json.features[j].feature_value);
					}
				}
			}
		}
		data_real.push(real_value);
	}
	data_real.pop();
	console.log("data_real");
	console.log(data_real);
	
	var p;
	// Conseguimos el array global para llenar el gráfico
	console.log("datasets_data");
	console.log(datasets_data);
	datasets_data = [];
	for (i in data_real){
		if (flag == 0){
			p = {data: data_real[i], label: features_chart[i], borderColor: getRandomColor(), fill: false,hidden: true};
		} else {
			p = {data: data_real[i], label: features_chart[i], borderColor: getRandomColor(), fill: false};
		}
		datasets_data.push(p);
	}
	datasets_data.pop();
	console.log("datasets_data");
	console.log(datasets_data);
}


function dropfeature(){
	var date = getTimes();
	myChart.destroy();
	showData(0,1);
}


function dropfeatureGeneral(){
	var date = getTimes();
	myChart.destroy();
	showDataGeneral(0,date);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MÉTODOS PARA GENERAR LOS GRÁFICOS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Gráfico Específico
function createEsp(){
	showData(1,1);
}


function createGra(){
	ctx = document.getElementById('line-chart-js').getContext('2d');
	myChart = new Chart(ctx, {
		type: 'line',
		data: {
		labels: labels_day,
		datasets: datasets_data
		},
		options: 
		{
		title: {
		  display: true,
		  text: 'Informe Estadística del Nodo '+nameIndicador("sel")+" y el Dispositivo "+nameIndicador("sel2"),
		  fontSize: 22,
		  padding: 30
		},
		legend: {
		  display: true,
		  position: 'bottom',
		  labels: {
			  // fontColor: 'rgb(255, 99, 132)',
			  padding: 20,
			  
		  }
		},
		scales: 
		{
		  yAxes: [{ 
			scaleLabel: {
			  display: true,
			  labelString: "Valores",
			  fontSize: 18,
			}
		  }],
		  xAxes: [{ 
			scaleLabel: {
			  display: true,
			  labelString: "Fechas",
			  position: 'right',
			  fontSize: 18,
			}
		  }]
		}
	  }
	});
}


// Gráfico General
function createGen(){
	showDataGeneral(1,1);
}


function createGraGeneral(){
	console.log("Crea el gráfico general");
	ctx = document.getElementById('line-chart-general').getContext('2d');
	console.log(myChart);
	myChart = new Chart(ctx, {
		type: 'line',
		data: {
		labels: labels_day,
		datasets: datasets_data
		},
		options: 
		{
		title: {
		  display: true,
		  text: 'Cuadro Comparativo del Indicador '+nameIndicador("indicadores")+" del Dispositivo "+nameIndicador("equipos_lista")+" Entre los distintos Nodos",
		  fontSize: 22,
		  padding: 30
		},
		legend: {
		  display: true,
		  position: 'bottom',
		  labels: {
			  // fontColor: 'rgb(255, 99, 132)',
			  padding: 20,
			  
		  }
		},
		scales: 
		{
		  yAxes: [{ 
			scaleLabel: {
			  display: true,
			  labelString: nameIndicador("indicadores"),
			  fontSize: 18,
			}
		  }],
		  xAxes: [{ 
			scaleLabel: {
			  display: true,
			  labelString: "Fechas",
			  position: 'right',
			  fontSize: 18,
			}
		  }]
		}
	  }
	});
}


function nameIndicador(Valor){
    var select = document.getElementById(Valor);
    var text = select.options[select.selectedIndex].innerText;
	return text;
}